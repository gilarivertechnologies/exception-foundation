<?php
/**
 * Contract for all exceptions implementing ApiException
 */

namespace Gila\LaravelApiHelpers\ExceptionFoundation\Contracts\Exceptions;

/**
 * Interface ApiExceptionContract
 * @package Gila\LaravelApiHelpers\ExecptionFoundation\Contracts\Exceptions
 */
interface ApiExceptionContract
{
    /**
     * Returns the array of errors in the exception
     *
     * @return array
     */
    public function getErrorBag(): array;
}
