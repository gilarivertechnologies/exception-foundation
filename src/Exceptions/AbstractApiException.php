<?php
/**
 * Parent APIException
 */

namespace Gila\LaravelApiHelpers\ExceptionFoundation\Exceptions;

use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * Class AbstractApiException
 * @package Gila\LaravelApiHelpers\ExecptionFoundation\Exceptions
 */
abstract class AbstractApiException extends HttpException
{
    /**
     * @var array
     */
    protected $errorBag;

    /**
     * AbstractApiException constructor.
     * @param string $message
     * @param int $code
     * @param array $errors
     */
    public function __construct(string $message, int $code = 400, array $errors = [])
    {
        $this->setErrorBag($errors);
        parent::__construct($code, $message);
    }

    /**
     * @return array
     */
    public function getErrorBag(): array
    {
        return $this->errorBag;
    }

    /**
     * @param array $errors
     */
    protected function setErrorBag(array $errors): void
    {
        $this->errorBag = $errors;
    }
}
