<?php
/**
 * Exception thrown when given form data is invalid
 */

namespace Gila\LaravelApiHelpers\ExceptionFoundation\Exceptions;

use Gila\LaravelApiHelpers\ExceptionFoundation\Contracts\Exceptions\ApiExceptionContract;

/**
 * Class FormValidationException
 * @package Gila\LaravelApiHelpers\ExecptionFoundation\Exceptions
 */
class FormValidationException extends AbstractApiException implements ApiExceptionContract
{
    /**
     * The default exception message
     *
     * @const string
     */
    protected const EXCEPTION_MESSAGE = 'The given data was invalid.';

    /**
     * FormValidationException constructor.
     * @param array $errors
     * @param string|null $message
     * @param int $code
     */
    public function __construct(array $errors = [], string $message = null, int $code = 400)
    {
        parent::__construct(($message) ?: self::EXCEPTION_MESSAGE, $code, $errors);
    }
}
