<?php
/**
 * Trait to handle ApiExceptions
 */

namespace Gila\LaravelApiHelpers\ExceptionFoundation\Traits;

use Exception;
use Gila\LaravelApiHelpers\ExceptionFoundation\Contracts\Exceptions\ApiExceptionContract;
use Illuminate\Http\JsonResponse;

/**
 * Trait HandlesApiExceptions
 * @package Gila\LaravelApiHelpers\ExceptionFoundation\Traits
 */
trait HandlesApiExceptions
{
    /**
     * @param $request
     * @param Exception $exception
     * @return JsonResponse
     */
    public function render($request, Exception $exception): JsonResponse
    {
        $response = [
            'errors' => 'Something went wrong.',
        ];
        $status = 400;

        if ($exception instanceof ApiExceptionContract) {
            $response['errorBag'] = $exception->getErrorBag();
        }

        if (config('app.debug')) {
            $response['exception'] = get_class($exception);
            $response['message'] = $exception->getMessage();
            $response['trace'] = $exception->getTrace();
        }

        if ($this->isHttpException($exception)) {
            $status = $exception->getStatusCode();
        }

        return response()->json($response, $status);
    }
}
