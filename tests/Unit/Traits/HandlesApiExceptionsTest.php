<?php
/**
 * Tests for trait HandlesApiExceptions
 */

namespace Gila\LaravelApiHelpers\ExceptionFoundation\Tests\Unit\Traits;

use Gila\LaravelApiHelpers\ExceptionFoundation\Exceptions\FormValidationException;
use Gila\LaravelApiHelpers\ExceptionFoundation\Tests\TestCase;
use Gila\LaravelApiHelpers\ExceptionFoundation\Traits\HandlesApiExceptions;
use Illuminate\Contracts\Container\Container;
use Illuminate\Foundation\Exceptions\Handler;
use Illuminate\Http\Request;

/**
 * Class HandlesApiExceptionsTest
 * @package Gila\LaravelApiHelpers\ExceptionFoundation\Tests\Unit\Traits
 */
class HandlesApiExceptionsTest extends TestCase
{
    /**
     * @var Handler
     */
    protected $handler;

    public function setUp()
    {
        parent::setUp();

        $this->handler = new class extends Handler {
            public function __construct()
            {
                parent::__construct(\Mockery::mock(Container::class));
            }

            use HandlesApiExceptions;
        };
    }

    public function testApiExceptionFullyParsed()
    {
        $requestMock = \Mockery::mock(Request::class);

        $exception = new FormValidationException([
            'first_error' => 'this is an error'
        ], null, 422);

        $response = $this->handler->render($requestMock, $exception);

        $jsonContent = json_decode($response->content());

        $this->assertSame(422, $response->getStatusCode());
        $this->assertSame('Something went wrong.', $jsonContent->errors);
        $this->assertSame(['first_error' => 'this is an error'], (array) $jsonContent->errorBag);
    }

    public function testAdditionalReportingForDebugApps()
    {
        $requestMock = \Mockery::mock(Request::class);

        $exception = new FormValidationException([
            'first_error' => 'this is an error'
        ], null, 422);

        $response = $this->handler->render($requestMock, $exception);

        $jsonContent = json_decode($response->content());

        $this->assertSame(FormValidationException::class, $jsonContent->exception);
        $this->assertSame('The given data was invalid.', $jsonContent->message);
    }

    public function testNonApiExceptionRenders()
    {
        $requestMock = \Mockery::mock(Request::class);

        $response = $this->handler->render($requestMock, new \Exception('Test Message'));

        $jsonContent = json_decode($response->content());

        $this->assertSame(400, $response->getStatusCode());
        $this->assertSame('Something went wrong.', $jsonContent->errors);
        $this->assertSame('Test Message', $jsonContent->message);
    }
}
